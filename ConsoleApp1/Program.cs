﻿Console.OutputEncoding = System.Text.Encoding.UTF8;

// điều kiện để ngừng vòng lặp
bool finishLoop = false;
int personInput;
do
{
    // isSuccess = false nếu như TryParse chuyển đổi không thành công,
    // nếu là số thì sẽ chuyển đổi thành công trong trường hợp này isSuccess = true.
    Console.WriteLine("vui lòng nhập số trong khoảng 1-2-3, nếu không nhập đúng tôi bắt bạn nhập cho đến khi chết");

    bool isSuccess = int.TryParse(Console.ReadLine(), out personInput);


    // giả sử rằng mình đang cần dữ liệu nhập vào là số 1 hoặc 2 hoặc 3,
    // thì điều kiện nghiễm nhiên (isSuccess == true &&  personInput >0 && personInput <4) => easy 

    // cập nhật điều kiện của vòng lặp, em nhớ ở phía trên không, mình đang bắt người ta nhập số và là 1 2 hoặc 3 ;
    finishLoop = isSuccess == true && personInput > 0 && personInput < 4;


    // sau khi cập nhật điều kiện thì có phải là sẽ biết người dùng đang nhập đúng hay sai (nếu đúng thì if, nếu sai lọt vào else)
    if (finishLoop)  
    {
        Console.WriteLine("bạn đã nhập đúng, tôi buông tha bạn");
    }
    else
    {
        Console.WriteLine("xin đừng cố nhập sai, hãy nhập 1 hoặc 2 hoặc ");
    }

} while (!finishLoop); //  nếu fishLoop = true => ngừng đúng không ?



//vậy thì dòng này luôn luôn được gọi khi đảm bảo số nhập là 1,2 ,3
//=> lúc này em chơi kéo búa bao , hay thu thập quặng gì đó làm gì chẳng được,
//nếu em chỉ dùng int.Parse thì không ổn bởi cứ nhập chữ là nó lại lỗi tè le.
Console.WriteLine(personInput);



// với cách làm như vậy anh có hướng dẫn thêm bài Scissor , rock, paper em pull về xem lại ha

//https://gitlab.com/next.dev/scissor_rock_paper


